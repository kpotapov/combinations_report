<?php 
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

/** 
	 *
	 * @return Array[] Returns information about the products in the order
	 *
	 * @param int $order_id Id order
	 *
	 */

function fn_combinations_report_get_order_info($order_id)
{
    $lang_code = CART_LANGUAGE;
    $products_order = db_get_hash_array(
    	"SELECT ?:order_details.*, ?:product_descriptions.product, ?:products.status as product_status FROM ?:order_details "
    	. "LEFT JOIN ?:product_descriptions ON ?:order_details.product_id = ?:product_descriptions.product_id AND ?:product_descriptions.lang_code = ?s "
    	. "LEFT JOIN ?:products ON ?:order_details.product_id = ?:products.product_id "
    	. "WHERE ?:order_details.order_id = ?i ORDER BY ?:product_descriptions.product",
    	'item_id', $lang_code, $order_id
    );
    $format_info = true;
    $skip_static_values = false;
    foreach ($products_order as $k => $v) {
        //Check for product existance
                if (empty($v['product'])) {
                    $products_order[$k]['deleted_product'] = true;
                } else {
                    $products_order[$k]['deleted_product'] = false;
                }

        $products_order[$k]['discount'] = 0;

        $v['extra'] = @unserialize($v['extra']);
        if ($products_order[$k]['deleted_product'] == true && !empty($v['extra']['product'])) {
            $products_order[$k]['product'] = $v['extra']['product'];
        } else {
            $products_order[$k]['product'] = fn_get_product_name($v['product_id'], $lang_code);
        }

        $products_order[$k]['company_id'] = empty($v['extra']['company_id']) ? 0 : $v['extra']['company_id'];

        if (!empty($v['extra']['discount']) && floatval($v['extra']['discount'])) {
            $products_order[$k]['discount'] = $v['extra']['discount'];
            $order['use_discount'] = true;
        }

        if (!empty($v['extra']['promotions'])) {
            $products_order[$k]['promotions'] = $v['extra']['promotions'];
        }

        if (isset($v['extra']['base_price'])) {
            $products_order[$k]['base_price'] = floatval($v['extra']['base_price']);
        } else {
            $products_order[$k]['base_price'] = $v['price'];
        }
        $products_order[$k]['original_price'] = $products_order[$k]['base_price'];

                // Form hash key for this product
        
        $products_order[$k]['cart_id'] = $v['item_id'];
        $deps['P_'.$products_order[$k]['cart_id']] = $k;

                // Unserialize and collect product options information

        if (!empty($v['extra']['product_options'])) {
            if ($format_info == true) {
                if (!empty($v['extra']['product_options_value'])) {
                    $products_order[$k]['product_options'] = $v['extra']['product_options_value'];
                } else {
                    $products_order[$k]['product_options'] = fn_get_selected_product_options_info($v['extra']['product_options'], $lang_code);
                }
            }
            $product_options_value = ($skip_static_values == false && !empty($v['extra']['product_options_value'])) ? $v['extra']['product_options_value'] : array();

            if (empty($v['extra']['stored_price']) || (!empty($v['extra']['stored_price']) && $v['extra']['stored_price'] != 'Y')) {
                
                // apply modifiers if this is not the custom price
         
                $products_order[$k]['original_price'] = fn_apply_options_modifiers($v['extra']['product_options'], $products_order[$k]['base_price'], 'P', $product_options_value, array('product_data' => $v));
            }
        }

        $products_order[$k]['extra'] = $v['extra'];
        $products_order[$k]['tax_value'] = 0;
        $products_order[$k]['display_subtotal'] = $products_order[$k]['subtotal'] = ($v['price'] * $v['amount']);
    }
    return $products_order;
}
function fn_combinations_report_get_report_statistics(&$table,$combinations)
{
    $table_condition = fn_get_table_condition($table['table_id'], true);
    $order_ids = fn_proceed_table_conditions($table_condition, '?:orders');

    $last_elm = end($table['intervals']);
    $first_elm = reset($table['intervals']);

    $interval_code = $first_elm['interval_code'];
    $time_start = $first_elm['time_from'];
    $time_end = $last_elm['time_to'];
    $new_data = array();
	

    foreach ($table['elements'] as $key_element => $element) {
	

        $a = $element['element_hash'];
        if (empty($element['auto_generated'])) {
            $element['request'] = fn_get_parameter_request($table['table_id'], $element['element_hash']);
        }
        $time_condition = db_quote(" timestamp BETWEEN ?i AND ?i", $time_start, $time_end);
        $group_condition = ' GROUP BY `interval`';

        if ($interval_code == 'year') {
            $add_field = db_quote(", DATE_FORMAT(FROM_UNIXTIME(timestamp), '%Y') as `interval`, timestamp");
        } elseif ($interval_code == 'month') {
            $add_field = db_quote(", DATE_FORMAT(FROM_UNIXTIME(timestamp), '%Y-%m') as `interval`, timestamp");
        } elseif ($interval_code == 'week') {
            $add_field = db_quote(", DATE_FORMAT(FROM_UNIXTIME(timestamp), '%Y-%m-%u') as `interval`, timestamp");
        } elseif ($interval_code == 'day') {
            $add_field = db_quote(", DATE_FORMAT(FROM_UNIXTIME(timestamp), '%Y-%m-%d') as `interval`, timestamp");
        } else {
            $add_field = db_quote(", 1 as `interval`, timestamp");
            $group_condition = '';
        }
	

        if ($table['display'] == 'order_amount') {
            $fields = !empty($element['fields']) ? $element['fields'] : 'SUM(total)';
            $tables = !empty($element['tables']) ? $element['tables'] : '?:orders';

            $data[$a] = db_get_hash_array("SELECT $fields as total $add_field FROM $tables WHERE $element[request] AND $time_condition $order_ids $group_condition", 'interval');
        } elseif ($table['display'] == 'order_number') {
            $data[$a] = db_get_hash_array("SELECT COUNT(total) as total $add_field FROM ?:orders WHERE $element[request] AND $time_condition $order_ids $group_condition", 'interval');
        } elseif ($table['display'] == 'shipping') {
		
            $data[$a] = db_get_hash_array("SELECT SUM(shipping_cost) as total $add_field FROM ?:orders WHERE $element[request] AND $time_condition $order_ids $group_condition", 'interval');
		
        } elseif ($table['display'] == 'discount') {
            $data[$a] = db_get_hash_array("SELECT SUM(subtotal_discount) as total, ?:order_details.extra $add_field FROM ?:order_details LEFT JOIN ?:orders ON ?:orders.order_id = ?:order_details.order_id WHERE $element[request] AND $time_condition $order_ids $group_condition", 'interval');
	
            foreach ($data[$a] as $int => $interval_data) {
                $extra = @unserialize($interval_data['extra']);
                if (!empty($extra['discount'])) {
                    $data[$a][$int]['total'] += $extra['discount'];
                }
                unset($interval_data['extra']);
                $data[$a][$int]['total'] = fn_format_price($data[$a][$int]['total']);
            }

        } elseif ($table['display'] == 'tax') {
             $all_taxes = db_get_hash_array("SELECT ?:order_data.data $add_field FROM ?:order_data LEFT JOIN ?:orders ON ?:orders.order_id = ?:order_data.order_id WHERE ?:order_data.type = 'T' AND $element[request] AND $time_condition $order_ids $group_condition", 'interval');
		

             foreach ($all_taxes as $int => $interval_data) {
                $data[$a][$int] = $interval_data;
                $data[$a][$int]['total'] = 0;
                $taxes = @unserialize($interval_data['data']);
                if (is_array($taxes)) {
                    foreach ($taxes as $tax_data) {
                        if (!empty($tax_data['tax_subtotal'])) {
                            $data[$a][$int]['total'] += $tax_data['tax_subtotal'];
                        }
                    }
                }
                unset($data[$a][$int]['data']);
                $data[$a][$int]['total'] = fn_format_price($data[$a][$int]['total']);
            }
        } elseif ($table['display'] == 'product_cost') {
            
            $data[$a] = db_get_hash_array("SELECT SUM(amount * price) as total $add_field FROM ?:order_details LEFT JOIN ?:orders ON ?:orders.order_id = ?:order_details.order_id WHERE item_id = ?i AND $element[request] AND $time_condition $order_ids $group_condition", 'interval', $combinations[$key_element-1]['item_id']);
	
        } elseif ($table['display'] == 'product_number') {
            $data[$a] = db_get_hash_array("SELECT SUM(amount) as total $add_field FROM ?:order_details LEFT JOIN ?:orders ON ?:orders.order_id = ?:order_details.order_id WHERE item_id = ?i AND $element[request] AND $time_condition $order_ids $group_condition", 'interval', $combinations[$key_element-1]['item_id']);
        }
	

        foreach ($table['intervals'] as $interval) {
            $b = $interval['interval_id'];
            if (isset($data[$a])) {
                foreach ($data[$a] as $interval_data) {
                    if ($interval_data['timestamp'] >= $interval['time_from'] && $interval_data['timestamp'] <= $interval['time_to']) {
                        $new_data[$a][$b] = $interval_data['total'];
                        break;
                    }
                }
            }
          
            if (!isset($new_data[$a][$b])) {
                $new_data[$a][$b] = 0;
            }
        }
	
    }

    return $new_data;
}
