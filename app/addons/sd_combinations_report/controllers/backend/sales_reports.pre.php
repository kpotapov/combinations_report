<?php 

/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

require_once(Registry::get('config.dir.functions') . 'fn.sales_reports.php');
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	if ($mode == 'update_table'){
		

		$_max_elements = 30000;
		$except_table_id = 666; 
		$key_element_request = key($_REQUEST['table_data']['elements']);

		if (($_REQUEST['table_data']['elements'][$key_element_request]['limit_auto'] > $_max_elements) && ($_REQUEST['table_data']['elements'][$key_element_request]['element_id'] == $except_table_id))
			$_REQUEST['table_data']['elements'][$key_element_request]['limit_auto'] = $_max_elements;
			
		
	}
}
if ($mode == 'update_table'){
	$depend_items = fn_get_depended();
	$binary = false;
	foreach($depend_items as $item){
		if($item['code'] == 'combinations')
			$binary = true;
		}

	if(!$binary){
	db_query('INSERT INTO ?:sales_reports_elements SET element_id=?i, code=?s, depend_on_it=?s',666,'combinations','Y');
	$depend_items = fn_get_depended();
	}

}
