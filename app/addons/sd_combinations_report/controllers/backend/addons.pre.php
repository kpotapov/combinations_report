<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if ($mode == 'update_status') {		
		if($_REQUEST['id'] == 'sd_combinations_report' && $_REQUEST['status'] == 'D'){
		db_query('UPDATE ?:sales_reports_table_elements SET element_id = ?i WHERE element_id = ?i', 12, 666);
		db_query('DELETE FROM ?:sales_reports_elements WHERE code=?s','combinations');	
		}			
	}
}
