<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/
use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'view') {
    $table = Tygh::$app['view']->getTemplateVars('table');
    $report = Tygh::$app['view']->getTemplateVars('report');

    if ($table['elements'][1]['code'] == 'combinations') {	

        $table_condition = fn_get_table_condition($table['table_id'], true);
        $order_ids = fn_proceed_table_conditions($table_condition, FN_SR_CONDITION);
        $limit = $table['elements'][1]['limit_auto'];

		$products_rule_ids = '';
        if (!empty($table_condition['product'])) {
        	$products_rule_ids .= db_quote(' AND b.product_id IN (?n)', $table_condition['product']);
        }
        if (!empty($table_condition['category'])) {
        	$_p_ids = db_get_fields('SELECT product_id FROM ?:products_categories WHERE category_id IN (?n)', $table_condition['category']);
             if (!empty($_p_ids)) {
             	$products_rule_ids .= db_quote(' AND b.product_id IN (?n)', $_p_ids);
             }
        }



        if ($table['elements'][1]['dependence'] == 'max_n') {
            $combinations = db_get_array("SELECT b.item_id, SUM(b.amount) as product_value, b.product_id, c.product , b.order_id FROM ?:order_details as b LEFT JOIN ?:orders as a ON b.order_id = a.order_id LEFT JOIN ?:product_descriptions as c ON b.product_id = c.product_id AND c.lang_code = ?s WHERE a.timestamp BETWEEN ?i AND ?i ?p ?p GROUP BY b.item_id ORDER BY product_value DESC LIMIT $limit", CART_LANGUAGE, $table['time_from'], $table['time_to'], $order_ids, $products_rule_ids);
        }
        if ($table['elements'][1]['dependence'] == 'max_p') {
            $combinations = db_get_array("SELECT b.item_id, SUM(b.price * b.amount) as product_value, b.product_id, b.order_id, c.product FROM ?:order_details as b LEFT JOIN ?:orders as a ON b.order_id = a.order_id LEFT JOIN ?:product_descriptions as c ON b.product_id = c.product_id AND c.lang_code = ?s WHERE a.timestamp BETWEEN ?i AND ?i ?p ?p GROUP BY b.item_id ORDER BY product_value DESC LIMIT $limit", CART_LANGUAGE, $table['time_from'], $table['time_to'], $order_ids, $products_rule_ids);
        }

        $i=0;
	
	
        foreach ($combinations as $key_combination => $combination) {
            $i++;
            $order = fn_combinations_report_get_order_info($combination['order_id']);
			$product_name = $order[$combination['item_id']]['product'];
			$product_id = $combination['product_id'];
			$table['elements'][$i]['element_id'] = $product_id;
            if (isset($order[$combination['item_id']]['product_options'])) {
                $options_string = '';

                foreach ($order[$combination['item_id']]['product_options'] as $option) {
                    $options_string=$options_string.", ".$option['option_name'].":".$option['variant_name'];
                }

                $table['elements'][$i]['full_description'] = $product_name.$options_string;
                $table['elements'][$i]['description'] ='<a href="' . fn_url("products.update?product_id=$product_id") . '">'.$product_name.$options_string."</a>";
            } else {
                $table['elements'][$i]['full_description'] = $product_name;
                $table['elements'][$i]['description'] ='<a href="' . fn_url("products.update?product_id=$product_id") . '">'.$product_name."</a>";
            }
   
            $table['elements'][$i]['product_ids'] = array($product_id);
            $table['elements'][$i]['request'] = "?:orders.order_id IN ('" . implode("', '", db_get_fields("SELECT order_id FROM ?:order_details WHERE item_id = ?i", $combination['item_id'])) . "')";
        }
foreach($table['elements'] as $key_element => $element){
  if($key_element>$i)
   unset($table['elements'][$key_element]);
}

        $_values = fn_combinations_report_get_report_statistics($table,$combinations);
	
			
        $_max = 0;

        foreach ($_values as $key_values => $values) {
                foreach ($values as $key_value => $value) {
                    if ($value > $_max) {
                        $_max = $value;
                    }
                }
            }
        $table['max_value'] = $_max;
        if ($table['type'] == 'T') {
			
			foreach($table['elements'] as $key_telement => $table_element){
				$table['elements'][$key_telement]['description'] = $key_telement.'. '.$table_element['description'];
			}
            $table['values'] = $_values;
            $table['totals'] = array();

            foreach ($_values as $v) {
                foreach ($v as $_k => $_v) {
                    $table['totals'][$_k] = empty($table['totals'][$_k]) ? $_v : ($table['totals'][$_k] + $_v);
                }
            }

            Tygh::$app['view']->assign('table', $table);
            Tygh::$app['view']->assign('report', $report);
        } elseif ($table['type'] == 'P') {
            $new_array = Tygh::$app['view']->getTemplateVars('new_array');	
			
			$i=0;
            foreach ($_values as $value) {
                $new_array['pie_data'][$i]['count']  = $value[1];
                $i++;
            }
	    	foreach ($new_array['pie_data'] as $key_element => $element){				
				if($key_element>$i-1)	
					unset($new_array['pie_data'][$key_element]);
			}
			
            foreach ($new_array['pie_data'] as $key_product=>$product) {
                if (strlen($table['elements'][$key_product+1]['full_description']) > FN_MAX_STR_LENGTH) {
                    $new_array['pie_data'][$key_product]['label'] = mb_strimwidth($table['elements'][$key_product+1]['full_description'], 0, FN_MAX_STR_LENGTH, '...');
                    $new_array['pie_data'][$key_product]['full_descr'] = $table['elements'][$key_product+1]['full_description'];
                } else {
                    $new_array['pie_data'][$key_product]['full_descr'] = $new_array['pie_data'][$key_product]['label'] = $table['elements'][$key_product+1]['full_description'];
                }
            }
            

            Tygh::$app['view']->assign('new_array', $new_array);
        } elseif ($table['type'] == 'B') {
          	$new_array = Tygh::$app['view']->getTemplateVars('new_array');
$i=0;
            foreach ($table['elements'] as $key => $value) {
                foreach ($_values[$value['element_hash']] as $k => $v) {
					$i++;
                    $_new_array[] = array(
                        'title' => $i.'. '.$value['full_description'],
                    	'full_descr' => $value['full_description'],
                        'value' => $v,
                        'color' => fn_sr_get_random_color()
                    );
                }
            }
            $new_array['title'] = $table['description'];
            $new_array['column_data'] = $_new_array;
			
            foreach ($new_array['column_data'] as $key_product=>$product) {
                if (strlen($product['title']) > FN_MAX_STR_LENGTH) {
                    $new_array['column_data'][$key_product]['title'] = mb_strimwidth($product['title'], 0, FN_MAX_STR_LENGTH, '...');
                }
            }
            Tygh::$app['view']->assign('new_array', $new_array);
        }
    }
}
