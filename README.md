# README #

### Module "Combination Report" ###

* Adds the object to be analyzed in reports on sales:combinations

### Installation ###

* Download archive
* Add module by module manager(CSCART)
* The report settings, select an object for analysis : combination

### Developer ###

* [Konstantin Potapov](https://bitbucket.org/kpotapov/)